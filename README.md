# Demosite

実習IIIで作成するデモサイトの共有リポジトリです。
<br>
<br>

## ToDo

やってほしいことだったり、現状の問題点をイシューチケットとして発行するから、各自ググったりして解決できそうだったら取り組んでね。

***

<br>

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/s1f101802847/demosite.git
git branch -M main
git push -uf origin main
```

***

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/integrations/)

***

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

***

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:9cd8110dc1e52a02fb7e1e81f7318d47?https://docs.gitlab.com/ee/user/clusters/agent/)

***



## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

<br>
<br>

## ステータス
ここにプロジェクトの進行度を書いていくよ。